TYPECHECK=poetry run mypy
LINT=poetry run pylint
LINT2=poetry run pycodestyle
BLACK=poetry run black
PACKAGES=.
LOG_DIR=tmp

lint_all: lint lint2 # black

typecheck:
	@echo "Typechecking with mypy version `poetry run mypy --version`"
	$(TYPECHECK) $(PACKAGES)

lint:
	@echo "Linting with pylint, version:"
	@poetry run pylint --version | sed 's/^/  /'
	$(LINT) $(PACKAGES)

lint2:
	@echo "Linting with pycodestyle version `poetry run pycodestyle --version` (`poetry run which pycodestyle`)"
	$(LINT2) $(PACKAGES)

black:
	@echo "Running the black formatter"
	$(BLACK) --line-length 79 $(PACKAGES)

help:
	@grep '^.*:.*#.*' Makefile | grep -v grep | sed 's/\(.*:\) .*#\(.*\)/\1\2/'
