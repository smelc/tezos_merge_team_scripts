Script to find about open MRs, classifying them by their assignee, author, or reviewer.

# Setup

Install dependencies with poetry:

```
poetry install
```

* Move `gl.cfg.MOVEME` to `gl.cfg`.
* Get [access token](https://gitlab.com/-/profile/personal_access_tokens]) (the scope
  `read_api` should suffice), and fill it in `private_token` field of `gl.cfg`.

# Usage

* `poetry run python main.py` to list ready and draft MRs,
  classifiying them by assignee.

Flags to change the behavior:

* `--no-self`. When classifying MRs by the assignee, ignore MRs whose author and
  assignee is the same person.
* `--classify-by assignee`. Classify MRs by the assignee, if any (the default).
* `--classify-by author`. Classify MRs by authors.
* `--state draft`. Only consider draft MRs.
* `--state ready`. Only consider ready MRs.
* `--state all`. Consider both draft and ready MRs (the default).
* `--origin mt`. Only consider MRs authored by merge team members.
* `--origin all`. Consider all MRs, no matter if the author is in the merge team or not (the default).

# Examples

* Find blockers (people in the _Assignee_ field): `poetry run python main.py --no-self`
* Find people under review load (people in _Reviewer_): `poetry run python main.py --no-self --classify-by reviewer`
* Find people that write too many MRs (parallelize too much):
  `poetry run python main.py --classify-by author`
