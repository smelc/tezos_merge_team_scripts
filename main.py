import argparse
import sys

from util import init_gitlab

STATE_FLAG = "--state"
PROJECT_ID = 3836952  # ID of tezos/tezos


def get_merge_team_members(project):
    rules = project.approvalrules.list()
    assert len(rules) == 1
    return rules[0].eligible_approvers


def show_draft(arg):
    if arg == "all":
        return "both ready and draft"
    elif arg == "draft":
        return "only draft"
    elif arg == "ready":
        return "ready (non draft)"
    else:
        raise Exception(f"Unrecognized {STATE_FLAG} value: {arg}")


def state_to_wip(s):
    """ Converts the --state argument to the 'wip' named argument of the GitLab API """
    if s == "all":
        return None
    elif s == "draft":
        return "yes"
    elif s == "ready":
        return "no"
    else:
        raise Exception(f"Unrecognized {STATE_FLAG} value: {s}")


def go(project, args) -> int:
    """ Method where MRs are requested and classified. Returns an error code. """
    assert args.origin in ["all", "mt"]
    origin = "all developers" if args.origin == "all" else "MT members"
    assert args.classify_by in ["assignee", "author", "reviewer"]
    ignore = "" if args.self_mrs else ", ignoring self-authored MRs"
    print(f"Listing {show_draft(args.state)} MRs of {origin}, by {args.classify_by}{ignore}")

    mt_members = get_merge_team_members(project)

    all_mrs = project.mergerequests.list(
        state='opened', wip=state_to_wip(args.state), all=True
    )
    mrs = {}
    id_to_user = {}
    mr_ids = []

    for mr in all_mrs:
        # See https://docs.gitlab.com/ee/api/merge_requests.html to get
        # from where these strings and field names come from.
        mr_ids.append(mr.iid)
        if args.classify_by == "author":
            assert args.self_mrs
            concerned = [mr.author]
            ids = [mr.author["id"]]
        elif args.classify_by in ["assignee", "reviewer"]:
            concerned = mr.assignees if args.classify_by == "assignee" else mr.reviewers
            if not concerned:
                continue  # MR has no assignee/reviewer, ignoring it
            ids = [u["id"] for u in concerned]
            if (not args.self_mrs) and (mr.author["id"] in ids):
                # MR has identical author and assignee/reviewer
                continue
        else:
            print(f"Unrecognized --classify-by value: {args.classify_by}", file=sys.stderr)
            return 1
        for user in concerned:
            key = user["id"]
            assert isinstance(key, int)
            id_to_user[key] = user
        if args.origin == "mt" and (not any([mt_member["id"] == key for mt_member in mt_members])):
            # Restrict to MR members
            continue
        for key in ids:
            mrs[key] = mrs.get(key, []) + [mr]

    all_mrs = list(mrs.items())
    all_mrs.sort(key=lambda pair: len(pair[1]), reverse=True)
    fmt = "{user:<20} {count:<10}: {mr_ids}" if args.ids else \
        "{user:<20} {count:<10}"
    for (id_, mrs) in all_mrs:
        print(fmt.format(
            user=id_to_user[id_]["name"],
            count=len(mrs),
            mr_ids=[mr.iid for mr in mrs]
        ))

    if args.ids:
        print("Merge request ids:")
        print("({})".format(" ".join(map(str, mr_ids))))
    return 0


def main() -> int:
    """ Parses arguments and then calls go """
    parser = argparse.ArgumentParser(description="Gitlab MR coordinator tools")

    parser.add_argument(
        "--classify-by",
        help='Group MRs by their author ("author") or by their assignee ("assignee", the default), or by reviewer ("reviewer")',
        dest="classify_by",
        default="assignee",
        choices=["assignee", "author", "reviewer"]
    )

    parser.add_argument(
        '--no-self',
        help='When classifying MRs by their assignees/reviewers, omit MRs whose author and assignee/reviewer is the same person',
        dest='self_mrs',
        action='store_false',
        default=True
    )

    parser.add_argument(
        "--origin",
        help='Show only MRs of members of the merge-team ("mt") or all MRs ("all", the default)',
        dest="origin",
        default="all",
        choices=["mt", "all"]
    )

    parser.add_argument(
        "--state",
        help='Show only MRs that are draft ("draft"), or that are ready ("ready"), or "all" of them (the default)',
        dest="state",
        default="all",
        choices=["draft", "ready", "all"]
    )

    parser.add_argument(
        "--ids",
        help="Show IDs of selected MRs",
        action="store_true"
    )

    args = parser.parse_args()

    if args.classify_by == "author" and not args.self_mrs:
        print('--classify-by=author and --no-self are exclusive. Remove one of them.', file=sys.stderr)
        return 1

    glab = init_gitlab()

    # Get a project by ID
    project = glab.projects.get(PROJECT_ID)

    return go(project, args)


if __name__ == "__main__":
    sys.exit(main())
